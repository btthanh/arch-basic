#!/bin/bash

ln -sf /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime
hwclock --systohc
sed -i '177s/.//' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "LC_ADDRESS=vi_VN" >> /etc/locale.conf
echo "LC_IDENTIFICATION=vi_VN" >> /etc/locale.conf
echo "LC_MEASUREMENT=vi_VN" >> /etc/locale.conf
echo "LC_MONETARY=vi_VN" >> /etc/locale.conf
echo "LC_NAME=vi_VN" >> /etc/locale.conf
echo "LC_NUMERIC=vi_VN" >> /etc/locale.conf
echo "LC_PAPER=vi_VN" >> /etc/locale.conf
echo "LC_TELEPHONE=vi_VN" >> /etc/locale.conf
echo "LC_TIME=vi_VN" >> /etc/locale.conf
echo "KEYMAP=us" >> /etc/vconsole.conf
echo "zentaplus" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 zentaplus.localdomain zentaplus" >> /etc/hosts
echo root:password | chpasswd

# You can add xorg to the installation packages, I usually add it at the DE or WM install script
# You can remove the tlp package if you are installing on a desktop or vm

pacman -S grub networkmanager network-manager-applet dialog wpa_supplicant mtools dosfstools reflector base-devel linux-headers avahi xdg-user-dirs xdg-utils gvfs gvfs-smb nfs-utils inetutils dnsutils bluez bluez-utils bluez-tools cups hplip alsa-utils pulseaudio bash-completion openssh rsync reflector acpi acpi_call tlp virt-manager qemu qemu-arch-extra edk2-ovmf bridge-utils dnsmasq vde2 openbsd-netcat iptables-nft ipset firewalld flatpak sof-firmware nss-mdns acpid os-prober ntfs-3g terminus-font git vim

# pacman -S --noconfirm xf86-video-amdgpu
# pacman -S --noconfirm nvidia nvidia-utils nvidia-settings

grub-install --target=i386-pc /dev/sda # replace sdx with your disk name, not the partition
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable cups.service
systemctl enable sshd
systemctl enable avahi-daemon
systemctl enable tlp # You can comment this command out if you didn't install tlp, see above
systemctl enable reflector.timer
systemctl enable fstrim.timer
systemctl enable libvirtd
systemctl enable firewalld
systemctl enable acpid

useradd -m btthanh
echo btthanh:password | chpasswd
usermod -aG libvirt btthanh

echo "btthanh ALL=(ALL) ALL" >> /etc/sudoers.d/btthanh


printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"




